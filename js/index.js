 $(function(){
                    $("[data-toggle='tooltip']").tooltip();
                    $("[data-toggle='popover']").popover();
                    $('.carousel').carousel({
                        interval: 3000
                    });

                    $('#contacto').on('show.bs.modal', function (e){
                        console.log('El Modal de Contacto se está mostrando');
                        $('#BtnContacto').removeClass("btn btn-warning");
                        $('#BtnContacto').addClass('btn btn-primary');
                        $('#BtnContacto').prop('disabled', true);  
                    });

                    $('#contacto').on('shown.bs.modal', function(e){
                        console.log('Se Mostro Modal de Contacto');
                    });

                    $('#contacto').on('hide.bs.modal', function(e){
                        console.log(' Modal de Contacto se oculta');
                    });

                    $('#contacto').on('hidden.bs.modal', function(e){
                        console.log('Se ocultó Modal de Contacto');
                        $('#BtnContacto').prop('disabled', false);
                    });

                    
                });